Other contributors: https://github.com/diVineProportion (WT rss feed crawler)


# Requirements
```
python3.8
python3.8-dev
virtualenv
```
# Dev environment
```
virtualenv -p python3.8 venv38
source venv38/bin/activate
pip install -r requirements.txt

cp x51/config.py.example x51/config.py

# set the flask secret in the x51/config.py file, field 'FLASK_KEY'

python create_all.py
python run.py
```

exit the virtual enviroment with `deactivate`
