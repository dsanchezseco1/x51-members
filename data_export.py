import sys
import json
import datetime
from flask_security import SQLAlchemyUserDatastore, utils
from x51 import create_app, db, models

app = create_app()
app.app_context().push()


user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)

def decode_entry(entry):
    return {c.name: str(getattr(entry, c.name)) for c in entry.__table__.columns}

def export_user(username):
    user = user_datastore.get_user(username)
    data = dict(
        username = user.username,
        member = decode_entry(user.member),
        attended_trainings = [decode_entry(tr) for tr in user.member.attended],
        advised_trainings = [decode_entry(tr) for tr in user.member.advised],
        missing_trainings = [decode_entry(tr) for tr in user.member.missing]
    )
    with open('export_'+username+'.txt', 'w') as outfile:
        json.dump(data, outfile, sort_keys=True, indent=2)


def highest_attendance(days=90):
    today = datetime.datetime.today()
    margin = datetime.timedelta(days = days)
    members = models.Member.query.filter_by(active=True)

    def tr_range(tr):
        return today - margin <= tr.date
    results = []
    for m in members:
        if m.join_date > today - margin:
            continue
        attended = [tr for tr in m.attended if tr_range(tr)]
        advised = [tr for tr in m.advised if tr_range(tr)]
        missing = [tr for tr in m.missing if tr_range(tr)]
        
        n_total = len(advised)+len(missing)+len(attended)

        if n_total < days/14:
            continue

        try:
            fom = len(attended)/(n_total)
        except ZeroDivisionError:
            fom = 0

        
        results.append(dict(
        username = m.username,
        attended = attended,
        advised = advised,
        missing = missing,
        fom = fom,
        ))
    
    sorted_results = sorted(results, key=lambda k: k['fom'], reverse=True)     
    for r in sorted_results:
        print (r['username'], r['fom'])


def user_training_attendance(uid, sim, geozone ='TZ1', days=90, t2=None):
    if not t2:
        end_time = datetime.datetime.today()
    else:
        end_time = today - datetime.timedelta(days = t2)

    margin = datetime.timedelta(days = days)
    user = models.Member.query.filter_by(id=uid).first()

    trainings = [t for t in models.Training.query.filter_by(sim=sim, geozone=geozone) if t.date >= end_time - margin] 
    n_all_trainings = len(trainings)
    attended = [t for t in user.attended if t.date >= end_time - margin and t.sim == sim and t.geozone == geozone]
    n_user_trainings = len(attended)
    attendance = 100*n_user_trainings/n_all_trainings
    print(n_all_trainings, n_user_trainings)
    print('Attendance of {} to {} trainings over the past {} days: {}%'.format(
        user.username,
        sim,
        days, attendance))


def get_country_count():
    members = models.Member.query.filter_by(active=True).all()
    from collections import Counter
    countries = Counter([m.location for m in members])
    return countries





