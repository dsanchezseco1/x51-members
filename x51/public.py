import os
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from flask import current_app as app
import flask_mobility
from flask_security import login_required, current_user

from x51.auth import officer_required, admin_required
from x51.models import Member, Training, User, Award
from .database import db


bp = Blueprint('public', __name__, url_prefix='/public')

@bp.route('/dcs_briefing')
def live_brief():
    is_officer = current_user.has_role('officer')
    return render_template('public/whiteboard.html', officer = is_officer)
