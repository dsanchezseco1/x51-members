import os
import uuid
from flask import (
    Blueprint, 
    flash, 
    g, 
    redirect, 
    render_template,
    request,
    url_for,
    send_from_directory,
    Markup
)
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
from werkzeug.datastructures import CombinedMultiDict
from werkzeug import secure_filename
from flask_security import login_required, current_user

from x51.auth import officer_required, admin_required
from x51.models import User, Sortie, Pilot, Flight
from x51.forms import NewSortieForm, NewFlightForm, NewSortiePilotForm, EditSortiePilotForm
from .database import db
from .helpers import Platforms

bp = Blueprint('sorties', __name__, url_prefix='/sorties')

STAT_VARS = [
        'air_kills',
        'assists',
        'ground_kills',
        'successes',
        'failures',
        'aircraft_losses',
        'xp']

RANKINGS = [
    {
        'red': 'Lieutenant',
        'blue': 'Leutnant',
        'min': 0,
        'max': 100,
    },
    {
        'red': 'Senior Lieutenant',
        'blue': 'Oberleutnant',
        'min': 100,
        'max': 300,
    },
    {
        'red': 'Captain',
        'blue': 'Hauptmann',
        'min': 300,
        'max': 600,
    },
    {
        'red': 'Major',
        'blue': 'Major',
        'min': 600,
        'max': 1000,
    },
    {
        'red': 'Lieutenant Colonel',
        'blue': 'Oberstleutnant',
        'min': 1000,
        'max': 1500,
    },
    {
        'red': 'Colonel',
        'blue': 'Oberst',
        'min': 1500,
        'max': 1e8,
    },

]


#index
@bp.route('/')
@login_required
def index():
    # return list of all sorties for all platforms. (Filter on front end)
    sorties = Sortie.query.all()
    return render_template('sorties/index.html', sorties= sorties)

#add a new sortie (to be done by mission lead)
@bp.route('/new', methods = ['GET', 'POST'])
@login_required
def new_sortie():
    form = NewSortieForm()
    if request.method == 'POST':
        pilot = Pilot.query.filter_by(
        user_id = current_user.id, 
        nationality = form.nationality.data,
        alive=True).first()
        if not pilot:
            flash("You have not created your virtual pilot for this nation. Please create one now", 'danger')
            return redirect(url_for('sorties.new_sortie'))

        sortie = Sortie(
            platform = form.platform.data,
            date = form.date.data,
            description = form.description.data,
            nationality = form.nationality.data,
            lead_id = current_user.id,
            lead_pilot_id = pilot.id,
            lead_pilot_name = pilot.name
        )
        db.session.add(sortie)
        db.session.commit()
        return redirect(url_for('sorties.sortie', sortie_id=sortie.id))
    return render_template('sorties/new_sortie.html', form=form)

#view for a particular sortie
@bp.route('/sortie/<int:sortie_id>')
@login_required
def sortie(sortie_id):
    sortie = Sortie.query.filter_by(id=sortie_id).first()
    #calculate stats on the fly:
    flights = [f for f in Flight.query.filter_by(sortie_id=sortie_id)]
    pilot_dict = {}
    for f in flights:
        pilot = Pilot.query.filter_by(id=f.pilot_id).first()
        pilot_name = pilot.name
        user_name = User.query.filter_by(id=pilot.user_id).first().username
        pilot_dict[f.pilot_id] = '{} ({})'.format(user_name, pilot_name) 
    
    if flights:
        stats = dict(
            lead = User.query.filter_by(id=sortie.lead_id).first().username,
            num_flights = len(flights),
            total_xp = sum([int(f.xp or 0) for f in flights]),
            air_kills = sum([int(f.air_kills or 0) for f in flights]),
            ground_kills = sum([int(f.ground_kills or 0) for f in flights]),
            aircraft_losses = sum([int(f.aircraft_losses or 0) for f in flights]),
            pilot_losses = sum([1 for f in flights if f.died == True])
        )
    else:
        stats = []
    return render_template(
        'sorties/sortie.html',
        sortie=sortie,
        stats=stats,
        flights=flights,
        pilot_dict = pilot_dict)

#add a flight to a sortie. To be done by pilot
@bp.route('/sortie/<int:sortie_id>/new', methods = ['GET','POST'])
@login_required
def new_flight(sortie_id):
    sortie = Sortie.query.filter_by(id=sortie_id).first()
    pilot = Pilot.query.filter_by(
        user_id = current_user.id, 
        nationality = sortie.nationality,
        alive=True).first()
    
    if not pilot:
        flash("You have not created your virtual pilot for this nation. Please create one now", 'danger')
        return redirect(url_for('sorties.sortie', sortie_id=sortie_id))
    form = NewFlightForm()
    if request.method == 'POST':
        flight = Flight(
            sortie_id = sortie_id,
            user_id = current_user.id,
            pilot_id = pilot.id,
            pilot_name = pilot.name,
            air_kills = form.air_kills.data or 0,
            assists = form.assists.data or 0,
            ground_kills = form.ground_kills.data or 0,
            successes = form.successes.data or 0,
            failures = form.failures.data or 0,
            aircraft_losses = form.aircraft_losses.data or 0,
            xp=0,
            died = form.died.data
        )
        #calculate the xp based off the stats
        flight.xp = calc_xp(flight)
        db.session.add(flight)
        
        for s in STAT_VARS:
            try:
                setattr(pilot, s, getattr(pilot,s)+getattr(flight, s))
            except TypeError as e:
                app.logger.error("NULLs found in DB: pilot id: {}, flight id: {}".format(pilot.id, flight.id))
                app.logger.error(e)

        #if pilot died, kill it in the db.
        if form.died.data:
            pilot.alive = False
        pilot = _update_rank(pilot)
        db.session.commit()
        flash('combat log created!', 'success')
        return redirect(url_for('sorties.sortie', sortie_id=sortie_id))
    return render_template(
        'sorties/new_flight.html',
        form=form,
        pilot_name=pilot.name)
        
#function for calculating the flight XP        
def calc_xp(f):
    xp_mods = dict(
        air_kills = 50,
        assists = 10,
        ground_kills = 10,
        successes = 50,
        failures = 20,
    )
    #xp (AK + ASSIST + GK + SUCCESS +FAILED)*PLANE LOST
    tally = [getattr(f,k)*v for k, v in xp_mods.items()]
    if f.aircraft_losses:
       nerf = 0.5*f.aircraft_losses
    else:
        nerf = 1
    return nerf*sum(tally) 

#view a user's own pilot    
@bp.route('/user_pilot/<int:user_id>', methods=['GET', 'POST'])
@login_required
def user_pilot(user_id):
    user = User.query.filter_by(id=user_id).first()
    pilots = Pilot.query.filter_by(user_id=user.id)
    n_flights = {p.id:len(Flight.query.filter_by(pilot_id=p.id).all()) for p in pilots}
        
    current_pilots = pilots.filter_by(alive=True).all()
    dead_pilots = pilots.filter_by(alive=False).all()
    return render_template(
        'sorties/user_pilot.html',
        user_id = user_id,
        username = user.username,
        dead_pilots=dead_pilots,
        current_pilots=current_pilots,
        n_flights = n_flights,
        rankings = RANKINGS)

@bp.route('/new_pilot', methods=['GET', 'POST'])
@login_required
def new_pilot():
    form = NewSortiePilotForm()
    if request.method == 'POST':
        alive_pilot = Pilot.query.filter_by(
            user_id=current_user.id,
            alive=True,
            nationality=form.nationality.data,
            platform = form.platform.data
            ).first()
        if alive_pilot:
            flash('You already have active pilot {} for {}'.format(
                alive_pilot.name, alive_pilot.nationality
            ), 'danger')
            return redirect(url_for('sorties.new_pilot'))
        new_pilot = Pilot(
            user_id = current_user.id,
            name = form.name.data,
            nationality = form.nationality.data,
            platform = form.platform.data,
            rank = 0,
            alive=True,
            xp=0
        )
        db.session.add(new_pilot)
        db.session.commit()
        flash('New pilot "{}" created'.format(new_pilot.name), 'success')
        return redirect(url_for('sorties.index'))
    return render_template('sorties/new_pilot.html', form=form)

@bp.route('/edit_pilot/<int:pilot_id>', methods=['GET', 'POST'])
@login_required
def edit_pilot(pilot_id):
    pilot = Pilot.query.filter_by(id=pilot_id).first()
    if current_user.id != pilot.user_id:
        flash('You do not own this pilot', 'danger')
        return redirect(url_for('sorties.pilot', pilot_id=pilot_id))
    form = EditSortiePilotForm()
    if form.validate_on_submit():
        pilot.name = form.name.data
        app.logger.debug(pilot.name+ form.name.data)
        db.session.commit()
        flash('Pilot name updated to {}'.format(pilot.name), 'success')
        return redirect(url_for('sorties.pilot', pilot_id=pilot_id))
    else:
        form.name.data = pilot.name
    return render_template('sorties/edit_pilot.html', form=form, pilot=pilot)

def _update_rank(pilot):
    xp = pilot.xp
    for i, rank in enumerate(RANKINGS):
        if xp >= rank['min'] and xp < rank['max']:
            pilot.rank = i
            break
    return pilot

def _delete_flight(flight):
    """reverts the pilots stats to previous when deleting a flight (combat log)"""
    pilot = Pilot.query.filter_by(id=flight.pilot_id).first()
                
    for s in STAT_VARS:
        try:
            setattr(pilot, s, getattr(pilot,s)-getattr(flight, s))
        except TypeError as e:
            app.logger.error("NULLs found in DB: pilot id: {}, flight id: {}".format(pilot.id, flight.id))
            app.logger.error(e)
    pilot.alive = True #pilot is either still alive or would have died in this flight. Will always be true on reverting.
    pilot = _update_rank(pilot)
    db.session.delete(flight)
    db.session.commit()
    
@bp.route('/delete_sortie/<int:sortie_id>', methods=['GET', 'POST'])
@login_required
def delete_sortie(sortie_id):
    sortie = Sortie.query.filter_by(id=sortie_id).first()
    flights = Flight.query.filter_by(sortie_id=sortie_id).all()
    if request.method == 'POST':
        if current_user.id == sortie.lead_id or current_user.has_role('admin'):
            for flight in flights:
                _delete_flight(flight)
            db.session.delete(sortie)
            db.session.commit()
            flash('Sortie {} deleted'.format(sortie_id), 'warning')
                
        else:
            flash('ERROR: Only lead can delete the sortie', 'danger')
        return redirect(url_for('sorties.index'))    
    return render_template('sorties/delete_sortie.html', sortie=sortie)

@bp.route('/flight/delete/<int:flight_id>', methods=['GET', 'POST'])
@login_required
def delete_flight(flight_id):
    flight = Flight.query.filter_by(id=flight_id).first()
    sortie_id = flight.sortie_id
    if request.method =='POST':
        lead_id = Sortie.query.filter_by(id=sortie_id).first().lead_id
        if current_user.id == flight.user_id or current_user.id == lead_id or current_user.has_role('admin'):
            _delete_flight(flight)
            flash('Combat log deleted', 'warning')
            return redirect(url_for('sorties.sortie', sortie_id=sortie_id))
            
        else:
            flash("ERROR: Only lead can delete other player's logs", 'danger')
    return render_template('sorties/delete_flight.html', flight=flight,sortie_id=sortie_id)

@bp.route('/help')
def help():
    return render_template('sorties/help.html')

@bp.route('/league/<string:platform>')
@login_required
def league(platform):
    pilots = Pilot.query.filter_by(alive=True, platform=platform).all()
    blue_pilot_data = []
    red_pilot_data = []

    red = 'Allies'
    blue = 'Axis'
    if platform == 'dcs':
        red = 'REDFOR'
        blue = 'BLUEFOR'

    for pilot in pilots:
        user = User.query.filter_by(id=pilot.user_id).first()
        data = {s:getattr(pilot, s) for s in STAT_VARS}
        data['user'] = user.username
        data['user_id'] = user.id
        data['id'] = pilot.id
        data['name'] = pilot.name
        data['rank'] = pilot.rank
        data['sorties'] = len(Flight.query.filter_by(pilot_id=pilot.id).all())
        if pilot.nationality == 'blue':
            blue_pilot_data.append(data)
        elif pilot.nationality == 'red':
            red_pilot_data.append(data)
        else:
            app.logger.warning('Unknown nationality {} found when rendering sortie league page')
    return render_template(
        'sorties/league.html',
        sides = {'blue': blue, 'red': red},
        all_pilots = {'blue': blue_pilot_data, 'red': red_pilot_data},
        platform = platform,
        rankings = RANKINGS)


@bp.route('/pilot/<int:pilot_id>')
@login_required
def pilot(pilot_id):
    pilot = Pilot.query.filter_by(id=pilot_id).first()
    flights = Flight.query.filter_by(pilot_id=pilot_id).all()
    user = User.query.filter_by(id=pilot.user_id).first()
    for flight in flights:
        flight.date = Sortie.query.filter_by(id=flight.sortie_id).first().date
    return render_template(
        'sorties/pilot.html',
        pilot=pilot,
        flights=flights,
        rankings = RANKINGS,
        user = user) 



    









