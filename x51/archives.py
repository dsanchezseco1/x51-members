import os
import uuid
import datetime
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
from werkzeug.datastructures import CombinedMultiDict
from werkzeug import secure_filename
from flask_security import login_required, current_user

from x51.auth import officer_required, admin_required
from x51.models import Archive
from x51.forms import NewArchiveForm
from .database import db
from .helpers import get_upload_fpath, delete_upload

bp = Blueprint('archives', __name__, url_prefix='/archives')

basedir = os.path.dirname(os.path.abspath(__file__))

@bp.route('/')
def archives():
    archives = Archive.query.all()
    return render_template("archives/index.html", archives=archives)

@bp.route('/mine')
@login_required
def my_archives():
    archives = Archive.query.filter_by(id=current_user.id)
    return render_template("archives/index.html", archives=archives)

@bp.route('/upload', methods = ['GET', 'POST'])
@login_required
@officer_required
def upload_archive():
    form = NewArchiveForm()
    if form.validate_on_submit():
        fname = form.file.data.filename
        ufname, fpath = get_upload_fpath(fname, folder='archives')
        form.file.data.save(fpath)
        
        archive = Archive(
            name = fname,
            description = form.description.data,
            file = ufname,
            user = current_user
        )
        db.session.add(archive)
        db.session.commit()
        return redirect(url_for('archives.archives'))
    return render_template('archives/add_archive.html', form=form)

@bp.route('/delete/<int:archive_id>', methods= ['GET', 'POST'])
@login_required
@officer_required
def delete_archive(archive_id):
    archive = Archive.query.filter_by(id=archive_id).first()
    if request.method == 'POST':
        if current_user.id == archive.user.id or current_user.has_role('admin'):
            delete_upload(archive.file, folder='archives')
            db.session.delete(archive)
            db.session.commit()
            flash('Archive file {} deleted'.format(archive.name), 'success')
            
        else:
            flash('ERROR: You do not own this file.', 'error')
        return redirect(url_for('archives.archives'))
    return render_template('archives/delete_archive.html', archive=archive)




