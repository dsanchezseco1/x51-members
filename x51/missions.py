import os
import uuid
import datetime
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
from werkzeug.datastructures import CombinedMultiDict
from werkzeug import secure_filename
from flask_security import login_required, current_user

from x51.auth import officer_required, admin_required
from x51.models import Mission
from x51.forms import NewMissionForm
from .database import db
from .helpers import get_upload_fpath, delete_upload

bp = Blueprint('missions', __name__, url_prefix='/missions')

basedir = os.path.dirname(os.path.abspath(__file__))

@bp.route('/')
def missions():
    missions = Mission.query.all()
    return render_template("missions/index.html", missions=missions)

@bp.route('/mine')
@login_required
def my_missions():
    missions = Mission.query.filter_by(id=current_user.id)
    return render_template("missions/index.html", missions=missions)

@bp.route('/upload', methods = ['GET', 'POST'])
@login_required
def upload_mission():
    form = NewMissionForm()
    if form.validate_on_submit():
        briefing_ufname = ''
        if form.briefing.data:
            briefing_ufname, briefing_fpath = get_upload_fpath(form.briefing.data.filename, folder='missions')
            form.briefing.data.save(briefing_fpath)
        
        mission_fname = form.mission.data.filename
        mission_ufname, mission_fpath = get_upload_fpath(mission_fname, folder='missions')
        form.mission.data.save(mission_fpath)
        mission = Mission(
            name = mission_fname,
            description = form.description.data,
            category = form.category.data,
            briefing = briefing_ufname,
            mission = mission_ufname,
            user = current_user
        )
        db.session.add(mission)
        db.session.commit()
        return redirect(url_for('missions.missions'))
    return render_template('missions/add_mission.html', form=form)

@bp.route('/delete/<int:mission_id>', methods= ['GET', 'POST'])
@login_required
def delete_mission(mission_id):
    mission = Mission.query.filter_by(id=mission_id).first()
    if not current_user.has_role('admin'):
        if current_user.id != mission.user.id:
            flash('ERROR: You do not own this file.', 'danger')
            return redirect(url_for('missions.missions'))

    if request.method == 'POST':
        delete_upload(mission.mission, folder='missions')
        if mission.briefing:
            delete_upload(mission.briefing, folder='missions')
        db.session.delete(mission)
        db.session.commit()
        flash('Mission {} deleted'.format(mission.name), 'success')
        return redirect(url_for('missions.missions'))
    return render_template('missions/delete_mission.html', mission=mission)




