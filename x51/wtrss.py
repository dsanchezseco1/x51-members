
### adapted from https://github.com/diVineProportion/war_thunder-rss/ ###
import os
import sys
from bs4 import BeautifulSoup
from os.path import getsize
from PIL import ImageFile
import datetime
from time import strptime
from lxml import etree
import urllib.request
import datetime
import re

from werkzeug.contrib.atom import AtomFeed
from x51.models import Feed
from x51 import db

def getsizes(uri):
    file = urllib.request.urlopen(uri)
    size = file.headers.get("content-length")
    if size: 
        size = int(size)
    p = ImageFile.Parser()
    while True:
        data = file.read(1024)
        if not data:
            break
        p.feed(data)
        if p.image:
            return size, p.image.size
    file.close()
    return(size, None)

def gen_wtrss(instance_path, cache_age=600):
    feed = Feed.query.filter_by(name='wt').first()
    updatefeed= False
    newfeed= False
    if not feed:
        newfeed = True
    elif feed and feed.date and datetime.datetime.now() > (feed.date + datetime.timedelta(seconds=cache_age)):
        updatefeed = True
    
    if updatefeed or newfeed:
        req = urllib.request.Request('http://warthunder.com/en/news/')
        req.add_header('User-Agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')
        f = urllib.request.urlopen(req)

        s = f.read()
        soup = BeautifulSoup(s, "lxml")

        rss = etree.Element("rss")
        channel = etree.SubElement(rss, "channel")

        title = etree.SubElement(channel, "title")
        title.text = "War Thunder News"

        link = etree.SubElement(channel, "link")
        link.text = "http://warthunder.com/en/news"

        description = etree.SubElement(channel, "description")
        description.text = "WarThunder News: RSS Feed"

        id_re = re.compile('/news/([0-9]*)-.*')

        for link in soup.find_all('div', "news-item"):
            item = etree.SubElement(channel, "item")

            try:
                rss_title = link.find("a", class_="news-item__title")
                rss_pdate = link.find("p", class_="news-item__additional-date")
                rss_dtext = link.find("div", class_="news-item__text")
                rss_image = link.find("div", class_="news-item__img")	
                rss_comme = link.find("a", class_="news-item__additional-comment")
                rss_atags = link.find("a", class_="news-item__tag")

                title_text = rss_title.get_text()
                adate_date = rss_pdate.get_text()
                dtext_text = rss_dtext.get_text()
                
                image_link = rss_image.img['src']
                comme_link = rss_comme['href']
                comme_text = rss_comme.get_text()
                atags_link = rss_atags['href']
                atags_text = rss_atags.get_text()
            except (AttributeError, TypeError) as e:
                pass
                
            """
            [Day of Month] [Month name, full version] [Year, full version]
            --> 
            [Day of Week], [Day of Month] [Month name, short version] [Year, full version] -
            [Hour 00-23] [Minute 00-59] [Second 00-59] [UTC offset]
            """
            a = rss_pdate.get_text().split(" ")
            b = a[1][0].upper() + a[1][1:3].lower()
            c = strptime(b,'%b').tm_mon
            d = datetime.datetime(int(a[2]), c, int(a[0]), 0, 0)
            pdate_text = d.strftime("%a, %d %b %Y %H:%M:%S GMT")
            
            e = rss_image.img['src']
            f = e.replace(" ", "%20")
            image_link = "http://static.warthunder.com/upload/image/" + f[37:]
            image_leng = getsizes(image_link)


            itemTitle = etree.SubElement(item, "title")
            itemTitle.text = title_text

            itemLink = etree.SubElement(item, "link")
            itemLink.text = "http://warthunder.com" + rss_title.get('href')
    
            itemDate = etree.SubElement(item, "pubDate")
            itemDate.text = pdate_text
            
            itemDescription = etree.SubElement(item, "description")
            itemDescription.text = dtext_text
            
            itemImage = etree.SubElement(item, "image")
            itemImage.text = image_link
            

        feed_data = etree.tostring(rss, encoding='utf-8', pretty_print=True)
        #with open(fpath, 'wb') as outfile:
        #    outfile.truncate()
        #    outfile.write('<?xml version="1.0" encoding="utf-8"?>\n'.encode('utf-8'))
        #    outfile.write(feed_data)
        if newfeed:
            feed = Feed(name='wt')

        feed.xml = feed_data
        feed.date = datetime.datetime.now()
        if newfeed:
            db.session.add(feed)
        db.session.commit()
        return feed
    else:
        return feed

    
            
