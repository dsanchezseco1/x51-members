#threaded functions for handling external data
import os
import sys
import time
import json
from datetime import datetime, timezone
from requests.auth import HTTPBasicAuth
import requests
from flask import jsonify

#get dcs server list from ED's game api
basedir = os.path.realpath(os.path.dirname(__file__))


def get_discord_json(dtype):
    file_path = os.path.join(basedir, "data",  "discord_"+dtype+".json")
    if os.path.isfile(file_path):
        with open(file_path, 'r') as infile:
            discord_json = json.load(infile)
            return discord_json
    else:
        return None


def generate_server_dict(server_list, conn_error=False):
    return {
        'servers': server_list,
        'last_updated': datetime.now(timezone.utc).strftime('%m/%d/%Y, %H:%M:%S'),
        'CONNECTION_ERROR': conn_error}


def get_dcs_servers(user, passw, cache_age = 120):
    if not user or not passw:
        print("username or password not set for DCS", file=sys.stderr)

    outfile_path = os.path.join(basedir, "data",  "dcs_servers.json")
    if os.path.isfile(outfile_path) and (time.time() - os.path.getmtime(outfile_path) < cache_age):
        with open(outfile_path, 'r') as outfile:
            server_json = json.load(outfile)
            return server_json
    else:
        #auth with server and grab endpoint
        url = 'https://www.digitalcombatsimulator.com/gameapi/serverlist'
        auth = HTTPBasicAuth(user, passw)
        #if not SC 200, wait and try again
        try:          
            r = requests.post(url, auth=auth)
            assert r.status_code == 200
        except AssertionError:
            with open(outfile_path, 'r') as outfile:
                server_json = json.load(outfile)
                server_json['CONNECTION_ERROR'] = True
            return server_json
        else:
            with open(outfile_path, 'w') as outfile:
                data = generate_server_dict(r.json())
                json.dump(data, outfile)
            return data
            

#depracated
def dcs_server_loop(user, passw, timeout = 120, max_retries=5):
    if not user or not passw:
        print("username or password not set for DCS", file=sys.stderr)
    outfile_path = os.path.join(basedir, "data",  "dcs_servers.json")
    #try not to DDOS...
    if timeout < 30:
        raise ValueError("timeout for dcs server list request too small.")

    data_file = os.path.join(basedir, "data", "dcs_servers.json")
    retries = 0 

    while True:
        #auth with server and grab endpoint
        url = 'https://www.digitalcombatsimulator.com/gameapi/serverlist'
        auth = HTTPBasicAuth(user, passw)
        #if not SC 200, wait and try again
        try:          
            r = requests.post(url, auth=auth)
            assert r.status_code == 200
        except AssertionError as e:
            #retry for max_retries before giving up. In which case, write error to output
            if retries < max_retries:
                print("dcs server request returned code {}".format(r.status_code), file=sys.stderr)
                retries +=1
            else:
                with open(outfile_path, 'w') as outfile:
                    json.dump(
                        {
                            'servers': ["CONNECTION ERROR"],
                            'last_updated': datetime.now(timezone.utc).strftime('%m/%d/%Y, %H:%M:%S')
                        }, outfile)

                return "Max Retries Exceeded: " + e
        #if all is well, write to file
        else:
            with open(outfile_path, 'w') as outfile:
                json.dump({
                        'servers': r.json(),
                        'last_updated': datetime.now(timezone.utc).strftime('%m/%d/%Y, %H:%M:%S')
                    }, outfile)
        #wait 
        time.sleep(timeout)
