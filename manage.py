from flask_script import Manager
from flask_migrate import MigrateCommand
from x51 import create_app
from x51.models import *


manager = Manager(create_app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
